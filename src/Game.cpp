/*-------------------------------------------------------------------------
Significant portions of this project are based on the Ogre Tutorials
- https://ogrecave.github.io/ogre/api/1.10/tutorials.html
Copyright (c) 2000-2013 Torus Knot Software Ltd

Manual generation of meshes from here:
- http://wiki.ogre3d.org/Generating+A+Mesh

*/

#include <exception>
#include <iostream>

#include "Game.h"


Game::Game() : ApplicationContext("OgreTutorialApp")
{
   this->mBoxAnimationState = 0;
}


Game::~Game()
{
    //cleanup in the reverse order of creation/initialization

}


void Game::setup()
{
    // do not forget to call the base first
    ApplicationContext::setup();

    addInputListener(this);

    // get a pointer to the already created root
    Root* root = getRoot();
    scnMgr = root->createSceneManager();

    // register our scene with the RTSS
    RTShader::ShaderGenerator* shadergen = RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

    setupCamera();

    setupLights();

    //setupBoxMesh();
    //setupBoxAnimation();

    setupNinjaMesh();
    setupNinjaAnimation();
}

void Game::setupCamera()
{
    // Create Camera
    Camera* cam = scnMgr->createCamera("myCam");

    //Setup Camera
    cam->setNearClipDistance(5);

    // Position Camera - to do this it must be attached to a scene graph and added
    // to the scene.
    SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    camNode->setPosition(200, 300, 400);
    camNode->lookAt(Vector3(0, 0, 0), Node::TransformSpace::TS_WORLD);
    camNode->attachObject(cam);

    // Setup viewport for the camera.
    Viewport* vp = getRenderWindow()->addViewport(cam);
    vp->setBackgroundColour(ColourValue(0, 0, 0));

    // link the camera and view port.
    cam->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));
}

void Game::setupNinjaMesh()
{
    // Try and load in one of the ogre test meshes.
    try
    {
      ninjaEntity = scnMgr->createEntity("ninja.mesh");
      ninjaEntity->setCastShadows(true);

      ninjaSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
      ninjaSceneNode->attachObject(ninjaEntity);
    }
    catch(Exception e)
    {
      e.what();
    }

}

void Game::setupBoxMesh()
{
  box = scnMgr->createEntity("box", "cube.mesh");
  box->setCastShadows(true);

  boxNode = scnMgr->getRootSceneNode()->createChildSceneNode();
  boxNode->attachObject(box);

  boxNode->setPosition(0,50,0);
  boxNode->setScale(0.5,0.5,0.5);
}


void Game::setupNinjaAnimation()
{
  mNinjaAnimationState = ninjaEntity->getAnimationState("Walk"); // original was Walk
  mNinjaAnimationState->setLoop(true);
  mNinjaAnimationState->setEnabled(true);
}

void Game::setupBoxAnimation()
{
  Real duration= 4.0;
	Real step = duration/4.0;
	Real x = 100, y = 50, z = 100;
	Real s = 0.5f;

	boxNode->setPosition(x,y,z);
	boxNode->setScale(s,s,s);

  Animation* animation = scnMgr->createAnimation("BoxAnim",duration);
	animation->setInterpolationMode(Animation::IM_SPLINE);
	NodeAnimationTrack* track = animation->createNodeTrack(0,boxNode);

	TransformKeyFrame* key;

  key = track->createNodeKeyFrame(0.0f);
  key->setTranslate(Vector3(-x, y,-z));
  key->setScale(Vector3(s,s,s));

  key = track->createNodeKeyFrame(step);
  key->setTranslate(Vector3( -x, y,z));
  key->setScale(Vector3(s,s,s));

  key = track->createNodeKeyFrame(2.0*step);
  key ->setTranslate(Vector3( x, y, z));
  key->setScale(Vector3(s,s,s));

  key = track->createNodeKeyFrame(3.0*step);
  key->setTranslate(Vector3(x, y, -z));
  key->setScale(Vector3(s,s,s));

  key = track->createNodeKeyFrame(4.0*step);
  key->setTranslate(Vector3(-x, y,-z));
  key->setScale(Vector3(s,s,s));

  mBoxAnimationState = scnMgr->createAnimationState("BoxAnim");
	mBoxAnimationState->setEnabled(true);
	mBoxAnimationState->setLoop(true);
}

/*TODO
Is this the place to do this?  Some tutorials say no (https://paginas.fe.up.pt/~ruirodrig/wiki/doku.php?id=teaching:djco:ogre3d:ogretutorial04animation)
others say yes (http://wiki.ogre3d.org/IntermediateTutorial1SourceCurrent). The yes is on the offial site.
*/
bool Game::frameRenderingQueued(const FrameEvent& evt)
{
  //method in parent is virtual and is called anyway

  //update the animation by the time before the last frame.
  //mBoxAnimationState->addTime(evt.timeSinceLastFrame);

  mNinjaAnimationState->addTime(evt.timeSinceLastFrame);

  return true;
}

bool Game::frameEnded(const Ogre::FrameEvent &evt)
{
  return true;
}


bool Game::frameStarted (const Ogre::FrameEvent &evt)
{
	//Be sure to call base class - otherwise events are not polled.
	ApplicationContext::frameStarted(evt);

	return true;
}

void Game::setupLights()
{
    // Setup Abient light
    scnMgr->setAmbientLight(ColourValue(255, 255, 255));
    scnMgr->setShadowTechnique(ShadowTechnique::SHADOWTYPE_STENCIL_MODULATIVE);

    // Add a spotlight
    Light* spotLight = scnMgr->createLight("SpotLight");

    // Configure
    spotLight->setDiffuseColour(0, 0, 1.0);
    spotLight->setSpecularColour(0, 0, 1.0);
    spotLight->setType(Light::LT_SPOTLIGHT);
    spotLight->setSpotlightRange(Degree(35), Degree(50));


    // Create a schene node for the spotlight
    SceneNode* spotLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    spotLightNode->setDirection(-1, -1, 0);
    spotLightNode->setPosition(Vector3(200, 200, 0));

    // Add spotlight to the scene node.
    spotLightNode->attachObject(spotLight);

    // Create directional light
    Light* directionalLight = scnMgr->createLight("DirectionalLight");

    // Configure the light
    directionalLight->setType(Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(ColourValue(0.4, 0, 0));
    directionalLight->setSpecularColour(ColourValue(0.4, 0, 0));

    // Setup a scene node for the directional lightnode.
    SceneNode* directionalLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    directionalLightNode->attachObject(directionalLight);
    directionalLightNode->setDirection(Vector3(0, -1, 1));

    // Create a point light
    Light* pointLight = scnMgr->createLight("PointLight");

    // Configure the light
    pointLight->setType(Light::LT_POINT);
    pointLight->setDiffuseColour(0.3, 0.3, 0.3);
    pointLight->setSpecularColour(0.3, 0.3, 0.3);

    // setup the scene node for the point light
    SceneNode* pointLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();

    // Configure the light
    pointLightNode->setPosition(Vector3(0, 150, 250));

    // Add the light to the scene.
    pointLightNode->attachObject(pointLight);

}

bool Game::keyPressed(const KeyboardEvent& evt)
{
  //  std::cout << "Got key event" << std::endl;
    if (evt.keysym.sym == SDLK_ESCAPE)
    {
        getRoot()->queueEndRendering();
    }
	
	//if (evt.keysym.sym == SDLK_UP) {
	//	mNinjaAnimationState = ninjaEntity->getAnimationState("Idle");
	//	mNinjaAnimationState->setLoop(true);
		//mNinjaAnimationState->setEnabled(true);
	//} dont think it should be in here as return true peraphs shuts down game lol
    return true;
}


bool Game::mouseMoved(const MouseMotionEvent& evt)
{
//	std::cout << "Got Mouse" << std::endl;
	return true;
}
